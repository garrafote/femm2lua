using System;
using Femm2Lua.Core.Document;
using System.Collections.Generic;
using Femm2Lua.Core.Model;
using System.Text.RegularExpressions;

namespace Femm2Lua.Core.Parse
{
	public class FecParser : FemmParser <FecDocument>
	{
		protected override FecDocument CreateNewDocument ()
		{
			return new FecDocument ();
		}
		
		override protected bool ParseSection (string section, string value, FecDocument document)
		{
			var success = base.ParseSection (section, value, document);
			
			if (success) {
				return true;
			}
			
			switch (section) {
			case "":
				break;
			}
			
			return success;
		}

		override protected void ParseBlockPropsSection (string value, FecDocument document)
		{
			var wrapperMatches = wrapperExp.Matches (value);
			//document.BlockProps = new List<ElectrostaticBlock> ();
//			
//			foreach (Match wrapperMatch in wrapperMatches) {
//				var items = wrapperMatch.Groups["items"].Value;
//				var conductor = new Block();
//				var itemsMatches = itemsExp.Matches (items);
//				
//				foreach (Match itemsMatch in itemsMatches) {
//					var key = itemsMatch.Groups["key"].Value;
//					var val = itemsMatch.Groups["value"].Value;
//					
//					switch (key) {
//					case "BlockName":
//						conductor.BlockName = Quote (val);
//						break;
//					case "ex":
//						conductor.ex = val;
//						break;
//					case "ey":
//						conductor.ey = val;
//						break;
//					case "qv":
//						conductor.qv = val;
//						break;
//					}
//				}
				
				//document.BlockProps.Add (conductor);
//			}
		}

		override protected void ParsePointPropsSection (string value, FecDocument document)
		{
			var wrapperMatches = wrapperExp.Matches (value);
			document.PointProps = new List<Point> ();
			
			foreach (Match wrapperMatch in wrapperMatches) {
				var items = wrapperMatch.Groups["items"].Value;
				var point = new Point();
				var itemsMatches = itemsExp.Matches (items);
				
				foreach (Match itemsMatch in itemsMatches) {
					var key = itemsMatch.Groups["key"].Value;
					var val = itemsMatch.Groups["value"].Value;
					
					switch (key) {
					case "PointName":
						point.PointName = Quote (val);
						break;
					case "vpr":
						point.First = val;
						break;
					case "qpr":
						point.Seccond = val;
						break;
					}
				}
				
				document.PointProps.Add (point);
			}
		}

		
		override protected void ParseBdryPropsSection (string value, FecDocument document)
		{
		}
	}
}

