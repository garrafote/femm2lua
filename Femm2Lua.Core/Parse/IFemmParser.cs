using System;
using Femm2Lua.Core.Document;

namespace Femm2Lua.Core
{
	public interface IFemmParser
	{
		FemmDocument Parse (string femmInfo);
	}
}

