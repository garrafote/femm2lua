using System;
using Femm2Lua.Core.Document;
using System.Collections.Generic;
using Femm2Lua.Core.Model;
using System.Text.RegularExpressions;
using Femm2Lua.Core.Model.Electrostatic;

namespace Femm2Lua.Core.Parse
{
	public class FeeParser : FemmParser <FeeDocument>
	{
		protected override FeeDocument CreateNewDocument ()
		{
			return new FeeDocument ();
		}

		override protected bool ParseSection (string section, string value, FeeDocument document)
		{
			var success = base.ParseSection (section, value, document);

			if (success) {
				return true;
			}

			switch (section) {
			case "ConductorProps":
				ParseConductorPropsSection (value, document);
				break;
			}

			return success;
		}

		override protected void ParseBlockPropsSection (string value, FeeDocument document)
		{
			var wrapperMatches = wrapperExp.Matches (value);
			document.BlockProps = new List<Block> ();
			
			foreach (Match wrapperMatch in wrapperMatches) {
				var items = wrapperMatch.Groups["items"].Value;
				var material = new Block();
				var itemsMatches = itemsExp.Matches (items);
				
				foreach (Match itemsMatch in itemsMatches) {
					var key = itemsMatch.Groups["key"].Value;
					var val = itemsMatch.Groups["value"].Value;
					
					switch (key) {
					case "BlockName":
						material.BlockName = Quote (val);
						break;
					case "ex":
						material.ex = val;
						break;
					case "ey":
						material.ey = val;
						break;
					case "qv":
						material.qv = val;
						break;
					}
				}
				
				document.BlockProps.Add (material);
			}
		}

		protected override void ParseBdryPropsSection (string value, FeeDocument document)
		{
			var wrapperMatches = wrapperExp.Matches (value);
			document.BdryProps = new List<Boundary> ();

			foreach (Match wrapperMatch in wrapperMatches) {
				var items = wrapperMatch.Groups["items"].Value;
				var boundary = new Boundary();
				var itemsMatches = itemsExp.Matches (items);
				
				foreach (Match itemsMatch in itemsMatches) {
					var key = itemsMatch.Groups["key"].Value;
					var val = itemsMatch.Groups["value"].Value;
					
					switch (key) {
					case "BdryName":
						boundary.BdryName = Quote (val);
						break;
					case "BdryType":
						boundary.BdryType = val;
						break;
					case "Vs":
						boundary.Vs = val;
						break;
					case "qs":
						boundary.Qs = val;
						break;
					case "c0":
						boundary.C0 = val;
						break;
					case "c1":
						boundary.C1 = val;
						break;
					}
				}
				
				document.BdryProps.Add (boundary);
			}
		}
		
		override protected void ParsePointPropsSection (string value, FeeDocument document)
		{
			var wrapperMatches = wrapperExp.Matches (value);
			document.PointProps = new List<Point> ();
			
			foreach (Match wrapperMatch in wrapperMatches) {
				var items = wrapperMatch.Groups["items"].Value;
				var point = new Point();
				var itemsMatches = itemsExp.Matches (items);
				
				foreach (Match itemsMatch in itemsMatches) {
					var key = itemsMatch.Groups["key"].Value;
					var val = itemsMatch.Groups["value"].Value;
					
					switch (key) {
					case "PointName":
						point.PointName = Quote (val);
						break;
					case "Vp":
						point.First = val;
						break;
					case "qp":
						point.Seccond = val;
						break;
					}
				}
				
				document.PointProps.Add (point);
			}
		}

		virtual protected void ParseConductorPropsSection (string value, FeeDocument document)
		{
			// TODO: Test if working properly with new global expressions and if then remove this line
			//var wrapperExp = new Regex (@"<BeginConductor>(?<items>(.(?!<BeginConductor>))*)<EndConductor>", RegexOptions.Singleline);
			//var itemsExp = new Regex (@"<(?<key>\w+)>\s*=\s*(""(?<value>.*)""|(?<value>\S+))", RegexOptions.Singleline);
			
			var wrapperMatches = wrapperExp.Matches (value);
			document.ConductorProps = new List<Conductor> ();
			
			foreach (Match wrapperMatch in wrapperMatches) {
				var items = wrapperMatch.Groups["items"].Value;
				var conductor = new Conductor();
				var itemsMatches = itemsExp.Matches (items);
				
				foreach (Match itemsMatch in itemsMatches) {
					var key = itemsMatch.Groups["key"].Value;
					var val = itemsMatch.Groups["value"].Value;
					
					switch (key) {
					case "ConductorName":
						conductor.ConductorName = Quote (val);
						break;
					case "Vc":
						conductor.Vc = val;
						break;
					case "qc":
						conductor.Qc = val;
						break;
					case "ConductorType":
						conductor.ConductorType = val;
						break;
					}
				}
				
				document.ConductorProps.Add (conductor);
				
			}
			
		}

	}
}

