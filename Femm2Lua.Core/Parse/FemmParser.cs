using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Collections.Generic;

using Femm2Lua.Core.Document;
using Femm2Lua.Core.Logging;
using Femm2Lua.Core.Model;

namespace Femm2Lua.Core.Parse
{
	public abstract class FemmParser <TDocument> : IFemmParser  where TDocument : FemmDocument
	{
		protected readonly Regex integerExp;
		protected readonly Regex floatingExp;
		protected readonly Regex scientificExp;
		protected readonly Regex wordExp;
		protected readonly Regex commentExp;
		protected readonly Regex itemsExp;
		protected readonly Regex wrapperExp;

		protected readonly Logger Logger;

		public FemmParser ()
		{
			Logger = Logging.Logger.Instance;

			integerExp = new Regex (@"(?<value>[+|-]?\d+)");
			floatingExp = new Regex (@"(?<value>[+|-]?\d*\.?\d+)");
			scientificExp = new Regex (@"(?<value>[+|-]?\d+([E|e][+|-]?\d+)?)");
			wordExp = new Regex (@"(?<value>\w+)");

			commentExp = new Regex (@"""(?<value>.*)""");
			itemsExp = new Regex (@"<(?<key>\w+)>\s*=\s*(""(?<value>.*)""|((?<value>[^<]+)\r\n))", RegexOptions.Singleline);
			wrapperExp = new Regex (@"<Begin\w+>(?<items>(.(?!<Begin\w+>))*)<End\w+>", RegexOptions.Singleline);

		}

		abstract protected TDocument CreateNewDocument ();

		public FemmDocument Parse (string femmInfo)
		{
			var document = CreateNewDocument ();

			var sectionExp = new Regex (@"\[(?<section>\w+)\]\s*=\s*(?<value>[^\[]+)", RegexOptions.Singleline);

			var matches = sectionExp.Matches (femmInfo);

			foreach (Match match in matches) {
				var section = match.Groups["section"].Value;
				var value = match.Groups["value"].Value;

				try {
					ParseSection (section, value, document);
				} catch (NotImplementedException ex) {
					Logger.Warn ( section + " parser not implemented!");
				}
			}

			return document;
		}

		virtual protected bool ParseSection (string section, string value, TDocument document)
		{
			var result = false;

			switch (section) {
			case "Format":
				ParseFormatSection (value, document);
				break;
			case "Precision":
				result = ParsePrecisionSection (value, document);
				break;
			case "MinAngle":
				result = ParseMinAngleSection (value, document);
				break;
			case "Depth":
				result = ParseDepthSection (value, document);
				break;
			case "LengthUnits":
				result = ParseLengthUnitsSection (value, document);
				break;
			case "ProblemType":
				result = ParseProblemTypeSection (value, document);
				break;
			case "Coordinates":
				result = ParseCoordinatesSection (value, document);
				break;
			case "Comment":
				result = ParseCommentSection (value, document);
				break;
			case "PointProps":
				ParsePointPropsSection (value, document);
				break;
			case "BdryProps":
				ParseBdryPropsSection (value, document);
				break;
			case "BlockProps":
				ParseBlockPropsSection (value, document);
				break;
			case "NumPoints":
				ParseNumPointsSection (value, document);
				break;
			case "NumSegments":
				ParseNumSegmentsSection (value, document);
				break;
			case "NumArcSegments":
				ParseNumArcSegmentsSection (value, document);
				break;
			case "NumHoles":
				ParseNumHolesSection (value, document);
				break;
			case "NumBlockLabels":
				ParseNumBlockLabelsSection (value, document);
				break;
			default:
				result = false;
				break;
			}

			return result;
		}

		virtual protected bool ParseMinAngleSection (string value, TDocument document)
		{
			var match = integerExp.Match (value);
			
			if (!match.Success) {
				return false;
			}
			
			document.MinAngle = Quote (match.Groups["value"].Value);
			
			return true;
		}
		
		virtual protected bool ParsePrecisionSection (string value, TDocument document)
		{
			var match = scientificExp.Match (value);
			
			if (!match.Success) {
				return false;
			}
			
			document.Precision = Quote (match.Groups["value"].Value);
			
			return true;
		}
		
		virtual protected bool ParseFormatSection (string value, TDocument document)
		{
			var match = integerExp.Match (value);
			
			if (!match.Success) {
				return false;
			}
			
			document.Format = Quote (match.Groups["value"].Value);
			
			return true;
		}

		virtual protected bool ParseDepthSection (string value, TDocument document)
		{
			var match = integerExp.Match (value);
			
			if (!match.Success) {
				return false;
			}
			
			document.Depth = Quote (match.Groups["value"].Value);
			
			return true;
		}

		virtual protected bool ParseLengthUnitsSection (string value, TDocument document)
		{
			var match = wordExp.Match (value);
			
			if (!match.Success) {
				return false;
			}
			
			document.LengthUnits = Quote (match.Groups["value"].Value);
			
			return true;
		}

		virtual protected bool ParseProblemTypeSection (string value, TDocument document)
		{
			var match = wordExp.Match (value);
			
			if (!match.Success) {
				return false;
			}
			
			document.ProblemType = Quote (match.Groups["value"].Value);
			
			return true;
		}

		virtual protected bool ParseCoordinatesSection (string value, TDocument document)
		{
			var match = wordExp.Match (value);
			
			if (!match.Success) {
				return false;
			}
			
			document.Coordinates = Quote (match.Groups["value"].Value);
			
			return true;
		}

		virtual protected bool ParseCommentSection (string value, TDocument document)
		{
			var match = commentExp.Match (value);
			
			if (!match.Success) {
				return false;
			}
			
			document.Comment = match.Groups["value"].Value;
			
			return true;
		}
		
		abstract protected void ParsePointPropsSection (string value, TDocument document);
		
		abstract protected void ParseBdryPropsSection (string value, TDocument document);
		
		abstract protected void ParseBlockPropsSection (string value, TDocument document);
		
		virtual protected void ParseNumPointsSection (string value, TDocument document)
		{
			var points = value.Split ('\n').Skip (1);
			document.NumPoints = new List<string[]> ();

			foreach (var pointStr in points) {
				var pointMatches = floatingExp.Matches (pointStr);

				if (pointMatches.Count != 5) {
					continue;
				}

				var point = new string[5];
				for (int i = 0; i < 5; i++) {
					point[i] = pointMatches[i].Groups["value"].Value;
				}

				document.NumPoints.Add (point);
			}
		}
		
		virtual protected void ParseNumSegmentsSection (string value, TDocument document)
		{
			var segments = value.Split ('\n').Skip (1);
			document.NumSegments = new List<string[]> ();
			
			foreach (var segmentStr in segments) {
				var segmentMatches = floatingExp.Matches (segmentStr);
				
				if (segmentMatches.Count != 7) {
					continue;
				}
				
				var segment = new string[7];
				for (int i = 0; i < 7; i++) {
					segment[i] = segmentMatches[i].Groups["value"].Value;
				}
				
				document.NumSegments.Add (segment);
			}
		}
		
		virtual protected void ParseNumArcSegmentsSection (string value, TDocument document)
		{
			throw new NotImplementedException ();
		}
		
		virtual protected void ParseNumHolesSection (string value, TDocument document)
		{
			throw new NotImplementedException ();
		}
		
		virtual protected void ParseNumBlockLabelsSection (string value, TDocument document)
		{
			var labels = value.Split ('\n').Skip (1);
			document.NumBlockLabels = new List<string[]> ();
			
			foreach (var labelStr in labels) {
				var labelMatches = floatingExp.Matches (labelStr);
				
				if (labelMatches.Count != 6) {
					continue;
				}
				
				var label = new string[6];
				for (int i = 0; i < 6; i++) {
					label[i] = labelMatches[i].Groups["value"].Value;
				}
				
				document.NumBlockLabels.Add (label);
			}
		}

		protected string Quote (string value)
		{
			return "'" + value + "'";
		}
	}
}

