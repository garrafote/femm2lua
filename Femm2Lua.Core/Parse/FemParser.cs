using System;
using Femm2Lua.Core.Document;
using System.Collections.Generic;
using Femm2Lua.Core.Model;
using System.Text.RegularExpressions;
using System.Linq;
using Femm2Lua.Core.Model.Magnectic;

namespace Femm2Lua.Core.Parse
{
	public class FemParser : FemmParser <FemDocument>
	{
		protected override FemDocument CreateNewDocument ()
		{
			return new FemDocument ();
		}
		
		override protected bool ParseSection (string section, string value, FemDocument document)
		{
			var success = base.ParseSection (section, value, document);
			
			if (success) {
				return true;
			}
			
			switch (section) {
			case "Frequency":
				ParseFrequencySection (value, document);
				break;
			case "ACSolver":
				ParseAcSolverSection (value, document);
				break;
			case "CircuitProps":
				ParseCircuitPropsSection (value, document);
				break;
			}
			
			return success;
		}

		virtual protected bool ParseFrequencySection (string value, FemDocument document)
		{
			var match = floatingExp.Match (value);
			
			if (!match.Success) {
				return false;
			}
			
			document.Frequency = Quote (match.Groups["value"].Value);
			
			return true;
		}

		virtual protected bool ParseAcSolverSection (string value, FemDocument document)
		{
			var match = integerExp.Match (value);
			
			if (!match.Success) {
				return false;
			}
			
			document.AcSolver = match.Groups["value"].Value;
			
			return true;
		}

		override protected void ParseBlockPropsSection (string value, FemDocument document)
		{
			var femDocument = document as FemDocument;
			var wrapperMatches = wrapperExp.Matches (value);
			document.BlockProps = new List<Block> ();
			
			foreach (Match wrapperMatch in wrapperMatches) {
				var items = wrapperMatch.Groups["items"].Value;
				var block = new Block();
				var itemsMatches = itemsExp.Matches (items);
				
				foreach (Match itemsMatch in itemsMatches) {
					var key = itemsMatch.Groups["key"].Value;
					var val = itemsMatch.Groups["value"].Value;
					
					switch (key) {
					case "BlockName":
						block.BlockName = Quote(val);
						break;
					case "Mu_x":
						block.MuX = val;
						break;
					case "Mu_y":
						block.MuY = val;
						break;
					case "H_c":
						block.Hc = val;
						break;
					case "H_cAngle":
						block.HcAngle = val;
						break;
					case "J_re":
						block.JRe = val;
						break;
					case "J_im":
						block.JIm = val;
						break;
					case "Sigma":
						block.Sigma = val;
						break;
					case "d_lam":
						block.LamD = val;
						break;
					case "Phi_h":
						block.PhiH = val;
						break;
					case "Phi_hx":
						block.PhiHX = val;
						break;
					case "Phi_hy":
						block.PhiHY = val;
						break;
					case "LamType":
						block.LamType = val;
						break;
					case "LamFill":
						block.LamFill = val;
						break;
					case "NStrands":
						block.NStrands = val;
						break;
					case "WireD":
						block.WireD = val;
						break;
					case "BHPoints":
						var lst = val.Split(new [] {'\n', '\r'}, StringSplitOptions.RemoveEmptyEntries).Skip(1);
						block.BHPoints = new List<string[]> ();
						foreach (var item in lst) {
							block.BHPoints.Add(item.Split(new [] {' ', ',', '\t'}, StringSplitOptions.RemoveEmptyEntries));
						}
						break;
					}
				}

				femDocument.BlockProps.Add (block);
			}
		}

		override protected void ParsePointPropsSection (string value, FemDocument document)
		{
			var wrapperMatches = wrapperExp.Matches (value);
			document.PointProps = new List<Point> ();
			
			foreach (Match wrapperMatch in wrapperMatches) {
				var items = wrapperMatch.Groups["items"].Value;
				var point = new Point();
				var itemsMatches = itemsExp.Matches (items);
				
				foreach (Match itemsMatch in itemsMatches) {
					var key = itemsMatch.Groups["key"].Value;
					var val = itemsMatch.Groups["value"].Value;
					
					switch (key) {
					case "PointName":
						point.PointName = Quote (val);
						break;
					case "A_re":
						point.First = val;
						break;
					case "I_re":
						point.Seccond = val;
						break;
					}
				}
				
				document.PointProps.Add (point);
			}
		}

		override protected void ParseBdryPropsSection (string value, FemDocument document)
		{
			var wrapperMatches = wrapperExp.Matches (value);
			document.BdryProps = new List<Boundary> ();
			
			foreach (Match wrapperMatch in wrapperMatches) {
				var items = wrapperMatch.Groups["items"].Value;
				var boundary = new Boundary();
				var itemsMatches = itemsExp.Matches (items);
				
				foreach (Match itemsMatch in itemsMatches) {
					var key = itemsMatch.Groups["key"].Value;
					var val = itemsMatch.Groups["value"].Value;
					
					switch (key) {
					case "BdryName":
						boundary.BdryName = Quote(val);
						break;
					case "BdryType":
						boundary.BdryType = val;
						break;
					case "A_0":
						boundary.A0 = val;
						break;
					case "A_1":
						boundary.A1 = val;
						break;
					case "A_2":
						boundary.A2 = val;
						break;
					case "Phi":
						boundary.Phi = val;
						break;
					case "c0":
						boundary.C0 = val;
						break;
					case "c0i":
						boundary.C0i = val;
						break;
					case "c1":
						boundary.C1 = val;
						break;
					case "c1i":
						boundary.C1i = val;
						break;
					case "Mu_ssd":
						boundary.Mu = val;
						break;
					case "Sigma_ssd":
						boundary.Sigma = val;
						break;

					}
				}

				document.BdryProps.Add (boundary);
			}
		}

		protected void ParseCircuitPropsSection (string value, FemDocument document)
		{
			var wrapperMatches = wrapperExp.Matches (value);
			document.CircuitProps = new List<Circuit> ();
			
			foreach (Match wrapperMatch in wrapperMatches) {
				var items = wrapperMatch.Groups["items"].Value;
				var circuit = new Circuit();
				var itemsMatches = itemsExp.Matches (items);
				
				foreach (Match itemsMatch in itemsMatches) {
					var key = itemsMatch.Groups["key"].Value;
					var val = itemsMatch.Groups["value"].Value;
					
					switch (key) {
					case "CircuitName":
						circuit.CircuitName = Quote(val);
						break;
					case "CircuitType":
						circuit.CircuitType = val;
						break;
					case "TotalAmps_re":
						circuit.TotalAmpsRe = val;
						break;
					case "TotalAmps_im":
						circuit.TotalAmpsIm = val;
						break;
					}
				}
				
				document.CircuitProps.Add (circuit);
			}
			
		}

	}
}

