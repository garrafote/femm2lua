using System;

namespace Femm2Lua.Core.Logging
{
	/// <summary>
	/// Possible log levels.
	/// More prioritized levels come first. 
	/// </summary>
	public enum LogLevel
	{
		/// <summary>
		/// This log level is never printed. It can be used to
		/// turn of the logger or dinamically turn off an specific
		/// log message.
		/// </summary>
		Off,

		/// <summary>
		/// The fatal level is used to log errors that can't be recovered.
		/// </summary>
		Fatal,

		/// <summary>
		/// The error level is used to log general errors.
		/// </summary>
		Error,

		/// <summary>
		/// The warn level is used to warn situations that can generate errors.
		/// </summary>
		Warn,

		/// <summary>
		/// The info level is used for information proposes.
		/// </summary>
		Info,

		/// <summary>
		/// The debug level is used for debugging proposes.
		/// </summary>
		Debug,

		/// <summary>
		/// The trace is used for detailed debugging.
		/// </summary>
		Trace
	}
}

