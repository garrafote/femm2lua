using System;
using System.IO;

namespace Femm2Lua.Core.Logging
{
	/// <summary>
	/// Simple logger class. this class doesn't manage its text writer.
	/// </summary>
	public class Logger
	{
		/// <summary>
		/// Gets or sets the text writer.
		/// </summary>
		/// <value>
		/// The text writer.
		/// </value>
		public TextWriter TextWriter { get; set; }

		/// <summary>
		/// Gets or sets the log level.
		/// Any log call above this level will be ignored.
		/// </summary>
		/// <value>
		/// The log level.
		/// </value>
		public LogLevel LogLevel { get; set; }

		
		#region Singleton

		/// <summary>
		/// Gets the logger instance.
		/// </summary>
		/// <value>
		/// The logger instance.
		/// </value>
		public static Logger Instance { get; private set; }

		/// <summary>
		/// Initializes the <see cref="Femm2Lua.Core.Logging.Logger"/> class.
		/// </summary>
		static Logger ()
		{
			Instance = new Logger ();
		}
		
		#endregion

		/// <summary>
		/// Initializes a new instance of the <see cref="Femm2Lua.Core.Logging.Logger"/> class.
		/// This class is meant to be singleton and you can access it by calling 
		/// <see cref="Femm2Lua.Core.Logging.Logger.Instance"/>
		/// </summary>
		protected Logger ()
		{
			TextWriter = Console.Out;
			LogLevel = LogLevel.Trace;
		}

		/// <summary>
		/// Log the message at the specified level.
		/// </summary>
		/// <param name='level'>
		/// Level of the current log.
		/// </param>
		/// <param name='message'>
		/// Message of the curret log.
		/// </param>
		public void Log (LogLevel level, string message)
		{
			if (level > LogLevel || level == LogLevel.Off) {
				return;
			}

			TextWriter.WriteLine(string.Format("[{0}] [{1}] {2}", DateTime.Now, level.ToString(), message));
			TextWriter.Flush();
		}

		/// <summary>
		/// Log the message at <see ref="Femm2Lua.Core.Logging.LogLevel.Info"/> level.
		/// </summary>
		/// <param name='message'>
		/// Message.
		/// </param>
		public void Info (string message)
		{
			Log (LogLevel.Info, message);
		}	

		/// <summary>
		/// Log the message at <see ref="Femm2Lua.Core.Logging.LogLevel.Trace"/> level.
		/// </summary>
		/// <param name='message'>
		/// Message.
		/// </param>
		public void Trace (string message)
		{
			Log (LogLevel.Trace, message);
		}

		/// <summary>
		/// Log the message at <see ref="Femm2Lua.Core.Logging.LogLevel.Debug"/> level.
		/// </summary>
		/// <param name='message'>
		/// Message.
		/// </param>
		public void Debug (string message)
		{
			Log (LogLevel.Debug, message);
		}

		/// <summary>
		/// Log the message at <see ref="Femm2Lua.Core.Logging.LogLevel.Error"/> level.
		/// </summary>
		/// <param name='message'>
		/// Message.
		/// </param>
		public void Error (string message)
		{
			Log (LogLevel.Error, message);
		}

		/// <summary>
		/// Log the message at <see ref="Femm2Lua.Core.Logging.LogLevel.Fatal"/> level.
		/// </summary>
		/// <param name='message'>
		/// Message.
		/// </param>
		public void Fatal (string message)
		{
			Log (LogLevel.Fatal, message);
		}

		/// <summary>
		/// Log the message at <see ref="Femm2Lua.Core.Logging.LogLevel.Warn"/> level.
		/// </summary>
		/// <param name='message'>
		/// Message.
		/// </param>
		public void Warn (string message)
		{
			Log (LogLevel.Warn, message);
		}
	}
}

