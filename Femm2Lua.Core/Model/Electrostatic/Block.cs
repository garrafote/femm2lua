using System;

namespace Femm2Lua.Core.Model.Electrostatic
{
	/// <summary>
	/// Block label representation.
	/// </summary>
	public struct Block
	{
		/// <summary>
		/// Gets or sets the name of the block material.
		/// </summary>
		/// <value>
		/// The name of the block label.
		/// </value>
		public string BlockName { get; set; }

		/// <summary>
		/// Gets or sets the Relative permittivity in the x- or r-direction.
		/// </summary>
		/// <value>
		/// the Relative permittivity in the x- or r-direction.
		/// </value>
		public string ex { get; set; }

		/// <summary>
		/// Gets or sets the Relative permittivity in the y- or z-direction.
		/// </summary>
		/// <value>
		/// The Relative permittivity in the y- or z-direction.
		/// </value>
		public string ey { get; set; }

		/// <summary>
		/// Gets or sets the Volume charge density in units of C/m3.
		/// </summary>
		/// <value>
		/// The Volume charge density in units of C/m3.
		/// </value>
		public string qv { get; set; }
	}
}

