using System;

namespace Femm2Lua.Core.Model.Electrostatic
{
	public class Boundary
	{
		public string BdryName { get; set; }
		public string BdryType { get; set; }
		public string Vs { get; set; }
		public string Qs { get; set; }
		public string C0 { get; set; }
		public string C1 { get; set; }
	}
}

