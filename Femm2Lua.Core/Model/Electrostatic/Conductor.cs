using System;

namespace Femm2Lua.Core.Model.Electrostatic
{
	/// <summary>
	/// Conductor representation.
	/// </summary>
	public struct Conductor
	{
		/// <summary>
		/// Gets or sets the name of the conductor.
		/// </summary>
		/// <value>
		/// The name of the conductor.
		/// </value>
		public string ConductorName { get; set; }

		/// <summary>
		/// Gets or sets the vc value.
		/// </summary>
		/// <value>
		/// The vc value.
		/// </value>
		public string Vc { get; set; }

		/// <summary>
		/// Gets or sets the qc value.
		/// </summary>
		/// <value>
		/// The qc value.
		/// </value>
		public string Qc { get; set; }

		/// <summary>
		/// Gets or sets the type of the conductor.
		/// </summary>
		/// <value>
		/// The type of the conductor.
		/// </value>
		public string ConductorType { get; set; }
	}
}

