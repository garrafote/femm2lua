using System;
using System.Collections.Generic;

namespace Femm2Lua.Core.Model.Magnectic
{
	public class Block
	{
		/// <summary>
		/// Gets or sets the name of the block.
		/// </summary>
		/// <value>
		/// The name of the block.
		/// </value>
		public string BlockName { get; set; }

		/// <summary>
		/// Gets or sets the Relative permeability in the x- or r-direction.
		/// </summary>
		/// <value>
		/// The Relative permeability in the x- or r-direction.
		/// </value>
		public string MuX { get; set; }

		/// <summary>
		/// Gets or sets the Relative permeability in the y- or z-direction.
		/// </summary>
		/// <value>
		/// The Relative permeability in the y- or z-direction.
		/// </value>
		public string MuY {get; set; }

		/// <summary>
		/// Gets or sets the Permanent magnet coercivity in Amps/Meter.
		/// </summary>
		/// <value>
		/// The Permanent magnet coercivity in Amps/Meter.
		/// </value>
		public string Hc { get; set; }

		// ??
		public string HcAngle { get; set; }

		/// <summary>
		/// Gets or sets the Real Applied source current density in Amps/mm2.
		/// </summary>
		/// <value>
		/// The Real Applied source current density in Amps/mm2.
		/// </value>
		public string JRe { get; set; }

		// imaginary part of J?
		public string JIm { get; set; }

		/// <summary>
		/// Gets or sets the Electrical conductivity of the material in MS/m.
		/// </summary>
		/// <value>
		/// The Electrical conductivity of the material in MS/m.
		/// </value>
		public string Sigma { get; set; }

		/// <summary>
		/// Gets or sets the Lamination thickness in millimeters.
		/// </summary>
		/// <value>
		/// The Lamination thickness in millimeters.
		/// </value>
		public string LamD { get; set; }

		/// <summary>
		/// Gets or sets the Hysteresis lag angle in degrees, used for nonlinear BH curves.
		/// </summary>
		/// <value>
		/// The Hysteresis lag angle in degrees, used for nonlinear BH curves
		/// </value>
		public string PhiH { get; set; }

		/// <summary>
		/// Gets or sets the phi Hysteresis lag in degrees in the x-direction for linear problems.
		/// </summary>
		/// <value>
		/// The phi Hysteresis lag in degrees in the x-direction for linear problems.
		/// </value>
		public string PhiHX { get; set; }

		/// <summary>
		/// Gets or sets Hysteresis lag in degrees in the y-direction for linear problems.
		/// </summary>
		/// <value>
		/// The phi Hysteresis lag in degrees in the y-direction for linear problems.
		/// </value>
		public string PhiHY { get; set; }

		/// <summary>
		/// Gets or sets the type of the lamination.
		/// <list type="bullet">
		/// 	<item><description>0 – Not laminated or laminated in plane</description></item> 
		/// 	<item><description>1 – laminated x or r</description></item>
		///		<item><description>2 – laminated y or z</description></item>
		///		<item><description>3 – Magnet wire</description></item>
		///		<item><description>4 – Plain stranded wire</description></item>
		///		<item><description>5 – Litz wire</description></item>
		///		<item><description>6 – Square wire</description></item>
		/// </list>
		/// </summary>
		/// <value>
		/// The type of the lamination.
		/// </value>
		public string LamType { get; set; }

		/// <summary>
		/// Gets or sets the The Fraction of the volume occupied per lamination that is actually 
		/// filled with iron.
		/// </summary>
		/// <value>
		/// The Fraction of the volume occupied per lamination that is actually filled with
		/// iron (Note that this parameter defaults to 1 the femme preprocessor dialog box because,
		///  by default, iron completely fills the volume).
		/// </value>
		public string LamFill { get; set; }

		/// <summary>
		/// Gets or sets the Number of strands in the wire build. Should be 1 for Magnet or Square wire.
		/// </summary>
		/// <value>
		/// The Number of strands in the wire build. Should be 1 for Magnet or Square wire.
		/// </value>
		public string NStrands { get; set; }

		/// <summary>
		/// Gets or sets the Diameter of each wire constituent strand in millimeters.
		/// </summary>
		/// <value>
		/// The Diameter of each wire constituent strand in millimeters.
		/// </value>
		public string WireD { get; set; }

		/// <summary>
		/// Gets or sets the B-H data points.
		/// </summary>
		/// <value>
		/// The B-H data points.
		/// </value>
		public IList<string[]> BHPoints { get; set; }

		public Block ()
		{
			BHPoints = new List<string[]> ();
		}
	}
}

