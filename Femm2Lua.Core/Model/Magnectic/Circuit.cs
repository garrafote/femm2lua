using System;

namespace Femm2Lua.Core
{
	public class Circuit
	{
		public string CircuitName { get; set; }
		public string TotalAmpsRe { get; set; }
		public string TotalAmpsIm { get; set; }
		public string CircuitType { get; set; }
	}
}

