using System;

namespace Femm2Lua.Core.Model.Magnectic
{
	public class Boundary
	{
//		BdryName
//		BdryType
//		A_0
//		A_1
//		A_2
//		Phi
//		c0
//		c0i
//		c1
//		c1i
//		Mu_ssd
//		Sigma_ssd
		// ("propname", A0, A1, A2, Phi, Mu, Sig, c0, c1, BdryFormat)
		public string BdryName { get; set; }
		public string BdryType { get; set; }
		public string A0 { get; set; }
		public string A1 { get; set; }
		public string A2 { get; set; }
		public string Phi { get; set; }
		public string C0 { get; set; }
		public string C0i { get; set; }
		public string C1 { get; set; }
		public string C1i { get; set; }
		public string Mu { get; set; }
		public string Sigma { get; set; }
	}
}

