using System;

namespace Femm2Lua.Core
{
	public class Point
	{
		/// <summary>
		/// Gets or sets the name of the point.
		/// </summary>
		/// <value>
		/// The name of the point.
		/// </value>
		public string PointName { get; set; }

		/// <summary>
		/// Gets or sets the first property.
		/// It can be a specified potential a in 
		/// units Webers/Meter for magnectic problems,
		/// a specified potential Vp for electrostatic 
		/// problems, a a specified temperature Tp for
		/// heatflow problems or a specified potential Vp
		/// for current flow problems
		/// </summary>
		/// <value>
		/// The first property.
		/// </value>
		public string First { get; set; }

		/// <summary>
		/// Gets or sets the seccond property.
		/// It can be a point current j in units of Amps
		/// for magnectic problems, a point charge density 
		/// qp in units of C/m for electrostatic problems,
		/// a point heat generation density qp in units of W/m
		/// for heatflow problems or a point current density qp
	    /// in units of A/m for current flow problems.
		/// </summary>
		/// <value>
		/// The seccond property.
		/// </value>
		public string Seccond { get; set; }
	}
}

