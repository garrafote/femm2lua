using System;
using System.Collections.Generic;
using Femm2Lua.Core.Model;
using Femm2Lua.Core.Model.Magnectic;

namespace Femm2Lua.Core.Document
{
	public class FemDocument : FemmDocument
	{
		#region implemented abstract members of FemmDocument
		
		public override int DocumentType {
			get {
				return 0;
			}
		}
		
		public override char DocumentPrefix {
			get {
				return 'm';
			}
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the list of block properties.
		/// </summary>
		/// <value>
		/// The list of block properties.
		/// </value>
		public IList<Block> BlockProps { get; set; }
		
		/// <summary>
		/// Gets or sets the list of boundary properties.
		/// </summary>
		/// <value>
		/// The list of boundary properties.
		/// </value>
		public IList<Boundary> BdryProps { get; set; }
		
		/// <summary>
		/// Gets or sets the list of circuit properties.
		/// </summary>
		/// <value>
		/// The list of circuit properties.
		/// </value>
		public IList<Circuit> CircuitProps { get; set; }
		


		/// <summary>
		/// Gets or sets the frequency.
		/// </summary>
		/// <value>
		/// The frequency.
		/// </value>
		public string Frequency { get; set; }

		/// <summary>
		/// Gets or sets the the solver type to
		/// be used for AC problems..
		/// </summary>
		/// <value>
		/// The the solver type to
		/// be used for AC problems..
		/// </value>
		public string AcSolver { get; set; }

		#endregion

		#region Structural Methods

		protected override void WriteProps ()
		{
			base.WriteProps ();
			
			WriteBlockProps ();
			WriteBdryProps ();
			WriteCircuitProps ();
		}

		#endregion

		#region Header Methods

		override protected void WriteProblemDefinition ()
		{
			var args = new [] {
				Frequency, 
				LengthUnits,
				ProblemType,
				Precision,
				Depth,
				MinAngle,
				AcSolver,
			};
			WriteProblemFunction ("i_probdef", args);
		}

		#endregion

		#region Properties Methods

		/// <summary>
		/// Writes a block of code containing block properties
		/// on the string builder.
		/// </summary>
		virtual protected void WriteBlockProps ()
		{
			BeginBlockComment ();
			WriteComment ("Block Properties");
			EndBlockComment ();
			
			foreach (var b in BlockProps) {
				var blockname = b.BlockName;

				var args = new [] {
					blockname, 
					b.MuX, 
					b.MuY, 
					b.Hc, 
					b.JRe, 
					b.Sigma, 
					b.LamD, 
					b.PhiH, 
					b.LamFill, 
					b.LamType, 
					b.PhiHX, 
					b.PhiHY,
					b.NStrands,
					b.WireD,
				};

				WriteComment(blockname);
				WriteProblemFunction ("i_addmaterial", args);
				foreach (var bhpoint in b.BHPoints) {
					if (bhpoint.Length < 2)
						continue;
					WriteProblemFunction("i_addbhpoint", blockname, bhpoint[0], bhpoint[1]);
				}

				builder.AppendLine ();
			}

			builder.AppendLine ();
		}

		/// <summary>
		/// Writes a block of code containing block properties
		/// on the string builder.
		/// </summary>
		virtual protected void WriteBdryProps ()
		{
			BeginBlockComment ();
			WriteComment ("Boundary Properties");
			EndBlockComment ();

			foreach (var b in BdryProps) {
				var args = new [] {
					b.BdryName,
					b.A0,
					b.A1,
					b.A2,
					b.Phi,
					b.Mu,
					b.Sigma,
					b.C0,
					b.C1,
					b.BdryType,
				};

				WriteProblemFunction ("i_addboundprop", args);
			}
			
			builder.AppendLine ();
		}

		/// <summary>
		/// Writes a block of code containing block properties
		/// on the string builder.
		/// </summary>
		virtual protected void WriteCircuitProps ()
		{
			BeginBlockComment ();
			WriteComment ("Circuit Properties");
			EndBlockComment ();
			
			foreach (var c in CircuitProps) {
				WriteProblemFunction ("i_addcircprop", c.CircuitName, c.TotalAmpsRe, c.CircuitType);
			}
			
			builder.AppendLine ();
		}


		#endregion

	}
}

