using System;
using System.Collections.Generic;
using Femm2Lua.Core.Model;
using Femm2Lua.Core.Model.Electrostatic;

namespace Femm2Lua.Core.Document
{
	public class FeeDocument : FemmDocument
	{
		/// <summary>
		/// Gets or sets the list of block properties.
		/// </summary>
		/// <value>
		/// The list of block properties.
		/// </value>
		public IList<Block> BlockProps { get; set; }
		
		/// <summary>
		/// Gets or sets the list of boundary properties.
		/// </summary>
		/// <value>
		/// The list of block properties.
		/// </value>
		public List<Boundary> BdryProps { get; set; }

		/// <summary>
		/// Gets or sets the list of conductor properties.
		/// </summary>
		/// <value>
		/// The list of conductor properties.
		/// </value>
		public IList<Conductor> ConductorProps { get; set; }


		#region implemented abstract members of FemmDocument

		public override int DocumentType {
			get {
				return 1;
			}
		}

		public override char DocumentPrefix {
			get {
				return 'e';
			}
		}

		#endregion

		
		/// <summary>
		/// Writes a block of code containing block properties
		/// on the string builder.
		/// </summary>
		virtual protected void WriteBlockProps ()
		{
			BeginBlockComment ();
			WriteComment ("Block Properties");
			EndBlockComment ();
			
			foreach (var b in BlockProps) {
				WriteProblemFunction ("i_addmaterial", b.BlockName, b.ex, b.ey, b.qv);
			}
			builder.AppendLine ();
		}

		/// <summary>
		/// Writes a block of code containing the conductor properties on 
		/// the string builder.
		/// </summary>
		virtual protected void WriteConductorProps ()
		{
			BeginBlockComment ();
			WriteComment ("Conductor Properties");
			EndBlockComment ();
			
			foreach (var c in ConductorProps) {
				WriteProblemFunction ("i_addconductorprop", c.ConductorName, c.Vc, c.Qc, c.ConductorType);
			}
			builder.AppendLine ();
		}

		virtual protected void WriteBdryProps ()
		{
			BeginBlockComment ();
			WriteComment ("Boundary Properties");
			EndBlockComment ();
			
			foreach (var b in BdryProps) {
				WriteProblemFunction ("i_addboundprop", b.BdryName, b.Vs, b.Qs, b.C0, b.C1, b.BdryType);
			}
			builder.AppendLine ();
		}

		protected override void WriteProps ()
		{
			base.WriteProps ();

			WriteConductorProps ();
			WriteBlockProps ();
			WriteBdryProps ();
		}
	}

}

