using System;
using System.IO;
using System.Text;
using System.Collections.Generic;

using Femm2Lua.Core.Logging;
using Femm2Lua.Core.Model;

namespace Femm2Lua.Core.Document
{
	/// <summary>
	/// The Femm Document holds all information extracted from femm files.
	/// This class is also responsable for generating lua scripts that reproduces
	/// its current data state.
	/// This is an abstract class and its subclasses should inform at least their
	/// document type and prefix.
	/// </summary>
	public abstract class FemmDocument
	{
		#region Properties

		/// <summary>
		/// Gets the type of the document.
		/// </summary>
		/// <value>
		/// The type of the document.
		/// </value>
		public abstract int DocumentType { get; }

		/// <summary>
		/// Gets the document prefix.
		/// </summary>
		/// <value>
		/// The document prefix.
		/// </value>
		public abstract char DocumentPrefix { get; }

		/// <summary>
		/// Gets or sets the format of the parsed femm file.
		/// </summary>
		/// <value>
		/// The format of the femm file.
		/// </value>
		public string Format { get; set; }

		/// <summary>
		/// Gets or sets the type of the problem.
		/// </summary>
		/// <value>
		/// The type of the problem.
		/// </value>
		public string ProblemType { get; set; }

		/// <summary>
		/// Gets or sets the length units.
		/// </summary>
		/// <value>
		/// The length units.
		/// </value>
		public string LengthUnits { get; set; }

		/// <summary>
		/// Gets or sets the problem depth.
		/// </summary>
		/// <value>
		/// The problem depth.
		/// </value>
		public string Depth { get; set; }

		/// <summary>
		/// Gets or sets the minimum angle.
		/// </summary>
		/// <value>
		/// The minimum angle in degrees.
		/// </value>
		public string MinAngle { get; set; }

		/// <summary>
		/// Gets or sets the precision.
		/// </summary>
		/// <value>
		/// The precision.
		/// </value>
		public string Precision { get; set; }

		/// <summary>
		/// Gets or sets the coordinates.
		/// </summary>
		/// <value>
		/// The coordinates (polar or cartesian).
		/// </value>
		public string Coordinates { get; set; }

		/// <summary>
		/// Gets or sets the comment.
		/// </summary>
		/// <value>
		/// The comment.
		/// </value>
		public string Comment { get; set; }

		/// <summary>
		/// Gets or sets the list of point properties.
		/// </summary>
		/// <value>
		/// The list of point properties.
		/// </value>
		public IList<Point> PointProps { get; set; }

		/// <summary>
		/// Gets or sets the list of numeric points.
		/// </summary>
		/// <value>
		/// The list of numeric points.
		/// </value>
		public IList<string[]> NumPoints { get; set; }

		/// <summary>
		/// Gets or sets the list of segments.
		/// </summary>
		/// <value>
		/// The list of segments.
		/// </value>
		public IList<string[]> NumSegments { get; set; }

		/// <summary>
		/// Gets or sets the list of block labels.
		/// </summary>
		/// <value>
		/// The list of block labels.
		/// </value>
		public IList<string[]> NumBlockLabels { get; set; }

		/// <summary>
		/// The string builder.
		/// </summary>
		protected readonly StringBuilder builder;

		/// <summary>
		/// The logger.
		/// </summary>
		protected readonly Logger Logger;
		
		#endregion

		#region Constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="Femm2Lua.Core.Document.FemmDocument"/> class.
		/// </summary>
		protected FemmDocument ()
		{
			Logger = Logging.Logger.Instance;
			builder = new StringBuilder ();
		}

		#endregion

		#region Helper/Base Methods

		/// <summary>
		/// Writes a function call using a document specific prefix.
		/// </summary>
		/// <param name='functionName'>
		/// Function name.
		/// </param>
		/// <param name='args'>
		/// Function arguments.
		/// </param>
		protected void WriteProblemFunction (string functionName, params string[] args)
		{
			builder.Append (DocumentPrefix);
			WriteFunction (functionName, args);
		}

		/// <summary>
		/// Writes a function call.
		/// <example>
		/// e.g. WriteFunction ("myFunction", "1", "'two'"); => myFunction(1, 'two')
		/// </example>			
		/// </summary>
		/// <param name='functionName'>
		/// Function name.
		/// </param>
		/// <param name='args'>
		/// Function arguments.
		/// </param>
		protected void WriteFunction (string functionName, params string[] args)
		{
			bool printSep = false;

			builder.Append (functionName);
			builder.Append ('(');
			
			foreach (var arg in args) {
				if (printSep) {
					builder.Append (", ");
				}
				builder.Append (arg);
				printSep = true;
			}
			
			builder.Append (')');
			builder.AppendLine ();
		}

		/// <summary>
		/// Begins the comment block.
		/// </summary>
		protected void BeginBlockComment ()
		{
			builder.Append ('-', 2);
			builder.Append ("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
			builder.AppendLine ();
		}

		/// <summary>
		/// Ends the comment block.
		/// </summary>
		protected void EndBlockComment ()
		{
			builder.Append ('-', 80);
			builder.AppendLine ();
		}

		/// <summary>
		/// Writes one line of comment.
		/// </summary>
		/// <param name='comment'>
		/// Comment to be written.
		/// </param>
		protected void WriteComment (string comment = "")
		{
			builder.Append ('-', 2);
			builder.Append (' ');
			builder.Append (comment);
			builder.AppendLine ();
		}

		#endregion

		#region Structural Methods

		/// <summary>
		/// Writes the lua script on the string builder.
		/// </summary>
		virtual protected void WriteLuaScript ()
		{
			WriteHeader ();
			
			WriteProps ();
			
			WriteElements ();
		}

		/// <summary>
		/// Writes the header.
		/// </summary>
		virtual protected void WriteHeader ()
		{
			BeginBlockComment ();
			WriteComment ("This script was generated by femm2lua, for more information access <ref>");
			EndBlockComment ();

			WriteFunction ("clearconsole");
			WriteFunction ("newdocument", DocumentType.ToString());
			builder.AppendLine ();

			WriteProblemDefinition ();
			builder.AppendLine ();
		}

		virtual protected void WriteProps ()
		{
			WritePointProps ();
		}

		
		virtual protected void WriteElements ()
		{
			WritePoints ();
			WriteSegments ();
			WriteBlockLabels ();
		}

		#endregion

		#region Header Methods

		/// <summary>
		/// Writes the problem definition.
		/// </summary>
		virtual protected void WriteProblemDefinition ()
		{
			WriteProblemFunction ("i_probdef", LengthUnits, ProblemType, Precision, Depth, MinAngle);
		}

		#endregion

		#region Properties Methods

		virtual protected void WritePointProps ()
		{
			BeginBlockComment ();
			WriteComment ("Point Properties");
			EndBlockComment ();
			
			foreach (var p in PointProps) {
				WriteProblemFunction ("i_addpointprop", p.PointName, p.First, p.Seccond);
			}
			builder.AppendLine ();
		}

		#endregion

		#region Elements Methods
		
		/// <summary>
		/// Writes a block of code containing the node definitions
		/// on the string builder.
		/// </summary>
		virtual protected void WritePoints ()
		{
			BeginBlockComment ();
			WriteComment ("Nodes");
			EndBlockComment ();
			
			foreach (var point in NumPoints) {
				WriteProblemFunction ("i_addnode", point[0], point[1]);
			}
			builder.AppendLine ();
		}
		
		/// <summary>
		/// Writes a block of code containing the block label definitions
		/// on the string builder.
		/// </summary>
		virtual protected void WriteBlockLabels ()
		{
			BeginBlockComment ();
			WriteComment ("Block Labels");
			EndBlockComment ();
			
			foreach (var label in NumBlockLabels) {
				WriteProblemFunction ("i_addblocklabel", label[0], label[1]);
			}
			builder.AppendLine ();
		}

		/// <summary>
		/// Writes a block of code containing the segment definitions
		/// on the string builder.
		/// </summary>
		virtual protected void WriteSegments ()
		{
			BeginBlockComment ();
			WriteComment ("Segments");
			EndBlockComment ();
			
			foreach (var segment in NumSegments) {
				var p1 = Int32.Parse (segment[0]);
				var p2 = Int32.Parse (segment[1]);
				
				WriteProblemFunction ("i_addsegment",
				                      NumPoints[p1][0], NumPoints[p1][1],
				                      NumPoints[p2][0], NumPoints[p2][1]);
			}
			builder.AppendLine ();
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Writes the script on a stream.
		/// </summary>
		/// <param name='stream'>
		/// Stream.
		/// </param>
		/// <param name='encoding'>
		/// Encoding to be used while writing.
		/// </param>
		public void WriteToStream (Stream stream, Encoding encoding)
		{
			WriteLuaScript ();
			
			var bytes = encoding.GetBytes (builder.ToString ());
			stream.Write (bytes, 0, bytes.Length);
		}
		
		/// <summary>
		/// Writes the script on a file using the specific encoding.
		/// </summary>
		/// <param name='filename'>
		/// Path of the file.
		/// </param>
		/// <param name='encoding'>
		/// Encoding to be used while writing.
		/// </param>
		public void WriteToFile (string filename, Encoding encoding)
		{
			WriteToStream (new FileStream(filename, FileMode.Create), encoding);
		}
		
		/// <summary>
		/// Writes the script on a file.
		/// </summary>
		/// <param name='filename'>
		/// Path of the file.
		/// </param>
		public void WriteToFile (string filename)
		{
			WriteToFile (filename, Encoding.Default);
		}

		#endregion

	}
}

