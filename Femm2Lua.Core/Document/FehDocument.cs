using System;

namespace Femm2Lua.Core.Document
{
	public class FehDocument : FemmDocument
	{
		#region implemented abstract members of FemmDocument
		
		public override int DocumentType {
			get {
				return 2;
			}
		}
		
		public override char DocumentPrefix {
			get {
				return 'h';
			}
		}
		
		#endregion
	}
}

