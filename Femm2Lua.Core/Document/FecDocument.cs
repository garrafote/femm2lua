using System;

namespace Femm2Lua.Core.Document
{
	public class FecDocument : FemmDocument
	{
		#region implemented abstract members of FemmDocument
		
		public override int DocumentType {
			get {
				return 3;
			}
		}
		
		public override char DocumentPrefix {
			get {
				return 'c';
			}
		}
		
		#endregion
	}
}

