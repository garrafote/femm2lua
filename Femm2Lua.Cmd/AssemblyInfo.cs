using System.Reflection;
using System.Runtime.CompilerServices;
using CommandLine.Text;

// Information about this assembly is defined by the following attributes. 
// Change them to the values specific to your project.
[assembly: AssemblyTitle("Femm2Lua Command Line Tool")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("Copyright (C) 2013 Garrafote")]
[assembly: AssemblyInformationalVersionAttribute("1.0")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// The assembly version has the format "{Major}.{Minor}.{Build}.{Revision}".
// The form "{Major}.{Minor}.*" will automatically update the build and revision,
// and "{Major}.{Minor}.{Build}.*" will update just the revision.

[assembly: AssemblyVersion("1.0.*")]

// The following attributes are used to specify the signing key for the assembly, 
// if desired. See the Mono documentation for more information about signing.

//[assembly: AssemblyDelaySign(false)]
//[assembly: AssemblyKeyFile("")]

// from CommandLineParser.Text

[assembly: AssemblyLicense(
	" ",
	"This is free software. You may redistribute copies of it under the terms of",
	"the MIT License <http://www.opensource.org/licenses/mit-license.php>.")]
[assembly: AssemblyUsage(
	" ",
	"Usage: femm2lua electro.fee",
	"       femm2lua mag.fem -p magnetic -o script.lua")]

