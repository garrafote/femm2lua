using System;
using System.IO;
using System.Text;
using System.Collections.Generic;

using CommandLine;
using CommandLine.Text;

using Femm2Lua.Core.Parse;
using Femm2Lua.Core.Logging;
using Femm2Lua.Core.Document;
using Femm2Lua.Core;

namespace Femm2Lua.Cmd
{
	class Program
	{
		public static void Main (string[] args)
		{

//			args = new string[] {
//				@"C:\Users\garrafote\Downloads\efi\trabalho3\trabalho3.fee", 
//				"-o", @"C:\Users\garrafote\Downloads\efi\trabalho3\output.lua",
//				"-v",
//			};

			args = new string[] {
				@"D:\femm2lua\Trabalho1.FEE", 
				"-o", @"D:\femm2lua\output.lua",
				"-v",
			};

			var options = new Options ();
			if (CommandLineParser.Default.ParseArguments (args, options)) {
				var prog = new Program ();
				prog.Run (options);
			}
		}

		private Logger logger;
		private IFemmParser parser;
		private FemmDocument document;

		public void Run (Options options)
		{
			logger = Femm2Lua.Core.Logging.Logger.Instance;
			logger.LogLevel = (options.Verbose) ? LogLevel.Info : LogLevel.Error;
			
			var femmInfo = File.ReadAllText (options.Input);

			CreateParser (options);
			document = parser.Parse (femmInfo);
			document.WriteToFile (options.Output);
		}

		void CreateParser (Options options)
		{
			if (options.ProblemType != null) {
				switch (options.ProblemType) {
				case "m":
				case "magnetic":
					parser = new FemParser ();
					break;
				case "e":
				case "electrostatic":
					parser = new FeeParser ();
					break;
				case "h":
				case "heatflow":
					parser = new FehParser ();
					break;
				case "c":
				case "currentflow":
					parser = new FecParser ();
					break;
				default:
					break;
				}
			}
			else {
				switch (options.Extension.ToLowerInvariant()) {
				case "fem":
					parser = new FemParser ();
					break;
				case "fee":
					parser = new FeeParser ();
					break;
				case "feh":
					parser = new FehParser ();
					break;
				case "fec":
					parser = new FecParser ();
					break;
				default:
					break;
				}
			}
		}
	}

	public class Options : CommandLineOptionsBase
	{
		[ValueList(typeof(List<string>), MaximumElements = 1)]
		public IList<string> InputList { get; set; }

		public string Input 
		{
			get {
				return InputList != null && InputList.Count > 0 ? InputList[0] : null;
			}
		}

		public string Extension
		{
			get {
				var input = Input;

				if (input == null) {
					return null;
				}

				var lastDot = input.LastIndexOf ('.');

				if (lastDot < 0) {
					return null;
				}

				return input.Substring (lastDot + 1);
			}
		}

		[Option("p", "problem-type", HelpText = "Problem tipe of the file to be parsed.")]
		public string ProblemType { get; set; }

		[Option("v", "verbose", HelpText = "Print details during execution.")]
		public bool Verbose { get; set; }

		[Option("o", "output", HelpText = "Output file name.")]
		public string Output { get; set; }

		[HelpOption("h", "help", HelpText = "Dispaly this help screen.")]
		public string GetUsage ()
		{
			return HelpText.AutoBuild(this, (HelpText current) =>
			    HelpText.DefaultParsingErrorsHandler(this, current));
		}
	}
}
